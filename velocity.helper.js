var fs = require('fs');
var velocity = require('velocityjs');

function clearCode(code) {
    // \r\n
    code = code.replace(/\r\n/g, '\n');

    // \t
    code = code.replace(/\t/g, '    ');

    // ##
    code = code.replace(/##.*/g, '');

    // #**#
    code = code.replace(/#\*(.*\n)*\*#/g, '');

    // Line End Space
    code = code.replace(/ +\n/g, '\n');

    // more \n
    code = code.replace(/\n{2,}/g, '\n\n');

    // head and end \n
    code = code.replace(/^\n+/g, '');
    code = code.replace(/\n+$/g, '\n');

    return code;
}

function getContext(vmString) {
    var ast = velocity.Parser.parse(vmString);
    var makeup = new velocity.Helper.Structure(ast);
    return makeup.context;
}

function getTemplate(path) {
    var html = fs.readFileSync(path, 'utf8');
    html = clearCode(html);
    return html;
}

module.exports = {
    clearCode,
    getContext,
    getTemplate,
    render: velocity.render
};
