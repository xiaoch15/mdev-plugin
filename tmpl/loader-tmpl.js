(function () {
    var modules = {};

    function isFunction(obj) {
        return Object.prototype.toString.call(obj) === '[object Function]';
    }

    function define(name, deps, factory) {
        if (modules[name]) {
            throw new Error('Module ' + name + ' has been defined already.');
        }

        if (isFunction(deps)) {
            factory = deps;
        }

        modules[name] = {
            factory: factory,
            inited: false,
            exports: null
        };
    }

    function run(name) {
        var module, exports, mod, ret;

        module = modules[name];
        exports = {};
        mod = {
            exports: {}
        };

        if (isFunction(module.factory)) {
            ret = module.factory.call(undefined, require, exports, mod);
            if (ret !== undefined) {
                module.exports = ret;
            } else {
                // mod.exports === {}
                if (mod.hasOwnProperty('exports') && typeof mod.exports === 'object' && mod.exports instanceof Object === true) {
                    var tag = false;
                    var k, v;
                    for (k in mod.exports) {
                        if (mod.exports.hasOwnProperty(k)) {
                            tag = true;
                        }
                    }
                    if (tag === false) {
                        module.exports = exports;
                    } else {
                        module.exports = mod.exports;
                    }
                } else {
                    module.exports = mod.exports;
                }
            }
        } else {
            throw new Error('Module ' + name + ' has no factory.');
        }

        module.inited = true;
    }

    function require(name) {
        var module;

        module = modules[name];

        if (!module) {
            throw new Error('Module ' + name + ' is not defined.');
        }

        if (module.inited === false) {
            run(name);
        }

        return module.exports;
    }

    DEFINE_MODULES

    RUN_FIRST_MODULE
}());
