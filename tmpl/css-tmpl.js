define(function (require, exports, module) {
    var cssText = decodeURIComponent('[[CSS-TEXT]]');

    var document = window.document;

    var styleTag = document.createElement('style');
    styleTag.id = 'seaJs-css';
    styleTag.setAttribute('type', 'text/css');
    var seaJsCss = document.getElementById('seaJs-css');
    if (seaJsCss) {
        seaJsCss.textContent += cssText;
        if (document.all) seaJsCss.styleSheet.cssText += cssText;
    } else if (document.all) {
        styleTag.styleSheet.cssText = cssText;
        document.getElementsByTagName("head").item(0).appendChild(styleTag);
    } else {
        styleTag.innerHTML = cssText;
        document.getElementsByTagName("head").item(0).appendChild(styleTag);
    }
});
