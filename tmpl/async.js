'use strict';

var res = '__resourceURL__';
var version = '__resourceVersion__';
var renderMode = '__renderMode__';

var loadStyle = function (url) {
    var style = window.document.createElement('link');
    style.setAttribute('type', 'text/css');
    style.setAttribute('rel', 'stylesheet');
    style.setAttribute('href', url);
    window.document.getElementsByTagName('head').item(0).appendChild(style);
};

var eMap = {};
var asyncLoadModule = function (moduleName, callback) {
    if (eMap[moduleName]) return;
    eMap[moduleName] = true;

    var fileType = (renderMode === 'online') ? '.min' : '.merge';

    var styleURI = res + 'module/' + moduleName + '/' + moduleName + fileType + '.css?' + version;
    var scriptURI = res + 'module/' + moduleName + '/' + moduleName + fileType + '.js?' + version;
    loadStyle(styleURI);
    window.$.getScript(scriptURI, function () {
        callback && callback();
    })
};

window.mdev = window.mdev || {};
window.mdev.asyncLoadModule = asyncLoadModule;
