'use strict';

const fs = require('fs');
const path = require('path');
const res = require('./resource.js');
const output = require('./output.js');
const mpath = require('./path.js');

function isObject(obj) {
    return Object.prototype.toString.call(obj) == '[object Object]';
}

function extend(defaultOptions, options) {
    var option;
    for (option in defaultOptions) {
        if (options.hasOwnProperty(option)) {
            if (isObject(defaultOptions[option])&&isObject(options[option])) {
                extend(defaultOptions[option], options[option]);
            }
        }
        else {
            options[option] = defaultOptions[option];
        }
    }
    return options;
}

function MdevPlugin(options) {
    var defaultOptions = {
        renderMode: 'test',
        pageRender: 'server',
        buildPath: mpath.build,
        packPath: mpath.pack,
        resourceVersion: +new Date(),
        resourceURL: 'http://127.0.0.1:3000/' + `${mpath.buildResource}`.replace('\.\/', '')
    };
    // this.options = extend(defaultOptions, options);
    this.options = Object.assign(options, defaultOptions);
}

MdevPlugin.prototype.apply = function(compiler) {
    var options = this.options;
    compiler.plugin('before-run', function(compilation, callback) {
        console.log('project build start...');

        // 解析project.conf -> 通过插件options设置参数
        // var proCxt = vmHelper.getContext(vmHelper.getTemplate(mpath.srcProjectConf));
        // proCxt.resourceVersion = +new Date();
        console.log('1: Get options -------------------------------------');
        console.log(options);

        // pub resource: 拷贝资源文件，并进行预处理
        console.log('2: Publish     -------------------------------------');
        res.publish(options.resourceURL, options.resourceVersion, options.renderMode);

        // output:       解析、编译layout/module/page/globalCxt，生成资源文件置于${buildPath}/resource/build下
        console.log('3: Output      -------------------------------------');
        output(options.resourceURL, options.resourceVersion, options.pageRender, options.renderMode);

        console.log('project build end.\nproject pack start...');
        callback();
    })
};

MdevPlugin.prototype.getEntries = function(wpDirname) {
    var getPages = () => {
        var pages = fs.readdirSync(path.resolve(wpDirname, 'src/page'));
        pages = pages.map(page => page.replace('.conf', ''));
        return pages;
    };

    var getPageEntriesByFile = () => {
        var pages = getPages();
        var pageEntries = {};
        for(var i=0;i < pages.length;i++) {
            pageEntries[`${pages[i]}`] = path.resolve(wpDirname, `${mpath.pack}${pages[i]}.js`);
        }
        return pageEntries;
    };

    var pageEntries = getPageEntriesByFile();
    var globalEntry = path.resolve(wpDirname, `${mpath.pack}global.js`);

    return Object.assign(pageEntries, {
        'global': globalEntry
    });
}

module.exports = MdevPlugin;