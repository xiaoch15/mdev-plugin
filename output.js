var fs = require('fs');
var path = require('path');

var mpath = require('./path.js');
var vmHelper = require('./velocity.helper.js');
var compiler = require('./compiler.js');

var unique = arr => Object.keys(arr.reduce((r, v) => { r[v] = true; return r; }, {}));

var getPageContext = (name, resourceURL, resourceVersion) => {
    var pageVars = Object.assign({
        pageName: name
    }, compiler.globalCxt, compiler.pagesObject[name]);

    var layout = compiler.layoutsObject[pageVars.layout];
    var cssList = [pageVars.moduleCss, layout.cssList];
    var jsList = [['script/async'], pageVars.moduleJs, layout.jsList];

    var pageModules = layout.modulesList.reduce((prev, modulesName) => {
        var modules = pageVars[modulesName] || [];
        prev[modulesName] = modules.map(moduleName => {
            var module = compiler.modulesObject[moduleName];
            cssList.push(module.cssList);
            jsList.push(module.jsList);
            return '#set($' + moduleName + ' = {})\n' + module.html;
        }).join('');
        return prev;
    }, {});
    // var mainHtml = proRes.proImageUrl(
    //     vmHelper.render('#macro(parseModules $modulesHtml)\n$modulesHtml\n#end' + layout.html, pageModules),
    //     resourceURL, resourceVersion
    // );
    var mainHtml = vmHelper.render('#macro(parseModules $modulesHtml)\n$modulesHtml\n#end' + layout.html, pageModules);
    var dataPage = fs.existsSync(`${mpath.srcData}data-${name}.vm`) ?
        fs.readFileSync(`${mpath.srcData}data-${name}.vm`, 'utf8')
            .replace(/\$!{resourceURL}/g, resourceURL).replace(/\$!{resourceVersion}/g, resourceVersion) :
        '';

    pageVars.mainHtml = `#set($resourceURL = "${resourceURL}")\n${dataPage}${mainHtml}`;

    pageVars.moduleCss = unique(cssList.reduce((prev, next) => prev.concat(next || []), []));
    pageVars.moduleJs = unique(jsList.reduce((prev, next) => prev.concat(next || []), []));

    return pageVars;
};

var createGlobalResFile = (pageVars, resourceURL) => {
    var buildPath = mpath.buildResource + 'build/';
    !fs.existsSync(buildPath) && fs.mkdirSync(buildPath);

    // global css
    var content = (pageVars.globalCss || []).map(v => `require("../${v}.css");\r\n`).join('');

    // global js
    content = content + (pageVars.globalJs || []).map(v => `require("../${v}.js");\r\n`).join('');
    fs.writeFileSync(path.resolve(buildPath + 'global.js'), content, 'utf8');
};

var createPageResFile = (pageVars, resourceURL) => {
    var buildPath = `${mpath.buildResource}build/`;
    !fs.existsSync(buildPath) && fs.mkdirSync(buildPath);

    // page css
    var content = pageVars.moduleCss.map(v => `require("../${v}.css");\r\n`).join('');

    // page js
    content = content + pageVars.moduleJs.map(v => `require("../${v}.js");\r\n`).join('');
    fs.writeFileSync(path.resolve(`${buildPath}${pageVars.pageName}.js`), content, 'utf8');
};

var run = (resourceURL, resourceVersion, pageRender, renderMode) => {
    var cssTemplate = vmHelper.getTemplate(mpath.tmplCss);
    var jsTemplate = vmHelper.getTemplate(mpath.tmplJs);
    var htmlTemplate = vmHelper.getTemplate(mpath.srcTemplate);

    // output page
    var pageVars;
    for (var page in compiler.pagesObject) {
        pageVars = getPageContext(page, resourceURL, resourceVersion);
        var tmplContext = Object.assign(pageVars, { pageRender, resourceURL, resourceVersion, renderMode });
        pageVars.resourceCss = vmHelper.render(cssTemplate, tmplContext);
        pageVars.resourceJs = vmHelper.render(jsTemplate, tmplContext);

        if (pageRender === 'client') {
            var dataJs = `${mpath.buildResource}data/data-${page}.js`;
            if (!fs.existsSync(dataJs)) {
                fs.writeFileSync(dataJs, `require('./tmpl-${page}.html');\n`, 'utf8');
            }

            pageVars.moduleJs.push(`data/data-${page}.js`);
            fs.writeFileSync(`${mpath.buildResource}data/tmpl-${page}.html`, pageVars.mainHtml, 'utf8');
            pageVars.mainHtml = '';
        }

        fs.writeFileSync(`${mpath.buildPage}${page}.` + (pageRender === 'client' ? 'html' : 'vm'), vmHelper.render(htmlTemplate, pageVars), 'utf8');

        console.log(`Output page ${page}`);
        createPageResFile(pageVars, resourceURL);
    }

    createGlobalResFile(pageVars, resourceURL);
};

module.exports = run;
