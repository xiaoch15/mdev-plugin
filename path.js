var pmdev = './mdev-plugin/';
var psrc = './src/';
var pbuild = './plugin-build/';

module.exports = {
    mdev           : `${pmdev}`,
    tmplCss        : `${pmdev}tmpl/exports.css.vm`,
    tmplJs         : `${pmdev}tmpl/exports.js.vm`,
    tmplJsPack     : `${pmdev}tmpl/wrapper.js.vm`,
    build          : `${pbuild}`,
    buildPage      : `${pbuild}page/`,
    buildResource  : `${pbuild}resource/`,
    src            : `${psrc}`,
    srcLayout      : `${psrc}layout/`,
    srcModule      : `${psrc}module/`,
    srcPage        : `${psrc}page/`,
    srcData        : `${psrc}data/`,
    srcCss         : `${psrc}css/`,
    srcScript      : `${psrc}script/`,
    srcImage       : `${psrc}image/`,
    srcGlobalConf  : `${psrc}conf/global_variable.conf`,
    srcProjectConf : `${psrc}conf/project.conf`,
    srcTemplate    : `${psrc}layout/template.html`,
    pack           : `${pbuild}resource/build/`,
};
