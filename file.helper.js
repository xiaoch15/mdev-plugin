var fs = require('fs');

var _pub = {};

_pub['cp'] = (from, to) => fs.writeFileSync(to, fs.readFileSync(from));

_pub['cpdir'] = function (from, to) {
    var files = [];

    if(fs.existsSync(to)) {
        _pub.rmdir(to);
    }
    fs.mkdirSync(to);

    if(!fs.existsSync(from)) {
        return;
    }

    files = fs.readdirSync(from);
    files.forEach(function(file) {
        var childPath = "/" + file;
        var fromPath = from + childPath;
        var toPath = to + childPath;
        if(fs.statSync(fromPath).isDirectory()) {
            _pub.cpdir(fromPath, toPath);
        } else {
            _pub.cp(fromPath, toPath);
        }
    });
};

_pub['rm'] = function (file) {
    fs.unlinkSync(file);
};

_pub['mv'] = function (from, to) {
    _pub.cp(from, to);
    _pub.rm(from);
};

_pub['rmdir'] = function (dir) {
    var files = [];

    if(!fs.existsSync(dir)) {
        return;
    }

    files = fs.readdirSync(dir);
    files.forEach(function(file) {
        var curPath = dir + '/' + file;
        if(fs.statSync(curPath).isDirectory()) {
            _pub.rmdir(curPath);
        } else {
            _pub.rm(curPath);
        }
    });

    fs.rmdirSync(dir);
};

_pub['mkdir'] = function (dir) {
    try {
        dir = dir.replace(/\\/g, '/');

        var dirs = dir.split('/');
        var i, path;
        for (i = 1; i <= dirs.length; i++) {
            path = dirs.slice(0, i).join('/');
            if(!fs.existsSync(path)) {
                fs.mkdirSync(path);
            } else {
                if (!fs.statSync(path).isDirectory()) {
                    console.log('error: "' + path + '" is not directory.');
                }
            }
        }
    } catch (e) {
        console.log('error: ' + dir + e);
    }
};

_pub['merge'] = function (fileList, toPath) {
    var content = '';

    fileList.forEach(function (v) {
        content += fs.readFileSync(v, 'utf8');
    });

    fs.writeFileSync(toPath, content, 'utf8');
};

module.exports = _pub;
