var fs = require('fs');
var path = require('path');
// var CleanCSS = require('clean-css');
// var uglifyjs = require('uglify-js');

var render = require('./velocity.helper.js').render;
var proRes = require('./process.resource.js');
var mpath = require('./path.js');
var file = require('./file.helper.js');

// const buildPath = mpath.buildResource + 'build/';

const buildAsyncFile = (resourceURL, resourceVersion, renderMode) =>
    fs.writeFileSync(
        mpath.buildResource + 'script/async.js',
        fs.readFileSync(path.resolve(__dirname, 'tmpl/async.js'), 'utf8')
            .replace('__resourceURL__', resourceURL)
            .replace('__resourceVersion__', resourceVersion)
            .replace('__renderMode__', renderMode),
        'utf8'
    );

const wrapJs = fileFullName => {
    fileFullName = fileFullName.replace(/\\/g, '/');

    if (['resource/lib/'].reduce((prev, next) =>
        (prev || (fileFullName.indexOf(next) > -1)), false))
        return;

    var dirname = path.dirname(fileFullName).replace(/\\/g, '/');
    var basename = path.basename(fileFullName, '.js');
    var modName, isOpenDom;

    if (dirname.indexOf('/' + basename) >= 0) {
        isOpenDom = true;
        modName = basename;
    }
    if (dirname.endsWith('/data')) {
        isOpenDom = true;
    }

    var tmpl = render(fs.readFileSync(mpath.tmplJsPack, 'utf8'), { modName: modName, dom: isOpenDom })
        .replace('/*code*/', '\n' + fs.readFileSync(fileFullName, 'utf8') + '\n');
    fs.writeFileSync(fileFullName, tmpl, 'utf8');
};

var publish = function (resURL, resVer, renderMode) {
    fs.existsSync(mpath.build) && file.rmdir(mpath.build);
    fs.mkdirSync(mpath.build);
    fs.mkdirSync(mpath.buildPage);
    fs.mkdirSync(mpath.buildResource);
    file.cpdir(path.resolve(mpath.src), path.resolve(mpath.buildResource));

    function walk(_path) {
        fs.readdirSync(_path).forEach(item => {
            var cPath = `${_path}/${item}`;
            if (fs.statSync(cPath).isDirectory()) {
                walk(cPath);
            } else {
                /\.html$/.test(item)            && proRes.proImageUrl(path.normalize(cPath), resURL, resVer);
                /\.js$/.test(item)              && wrapJs(path.normalize(cPath));
                /\.css$/.test(item)             && proRes.proCss(path.normalize(cPath), resURL, resVer);
            }
        });
    }
    walk(mpath.buildResource);

    // 创建必备目录
    !fs.existsSync(mpath.buildResource + 'data') && fs.mkdirSync(mpath.buildResource + 'data');

    // async file
    buildAsyncFile(resURL, resVer, renderMode);
};

module.exports = {
    publish
};
