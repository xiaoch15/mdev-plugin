var fs = require('fs');

var mpath = require('./path.js');
var vmHelper = require('./velocity.helper.js');

var compileLayout = (name, path) => {
    var html = vmHelper.getTemplate(`${path}/${name}.html`);
    var resource = vmHelper.getContext(html).resource || {};

    return {
        name: name,
        html: html,
        cssList: (resource.css || []).map(v => v.replace('./', `layout/${name}/`)),
        jsList: (resource.js || []).map(v => v.replace('./', `layout/${name}/`)),
        modulesList: (html.match(/\s#parseModules\(.*\)/g) || []).map(v => /\(\$(.*)\)/.exec(v)[1])
    };
};

var compileModule = (name, path) => {
    var ret = {
        name: name,
        html: vmHelper.getTemplate(`${path}/pagelet.html`),
        children: [],
        cssList: fs.existsSync(`${path}/${name}.css`) ? [`module/${name}/${name}`] : [],
        jsList: fs.existsSync(`${path}/${name}.js`) ? [`module/${name}/${name}`] : []
    };

    // #parseTmpl()
    var tmpls = ret.html.match(/\s#parseTmpl\(.*\)/g) || [];
    tmpls.forEach(val => {
        var tmplName = /\(['"]([^'"]*)['"]\)/.exec(val)[1];
        var tmpl = fs.readFileSync(`${mpath.src}${tmplName}`, 'utf8');
        ret.html = ret.html.replace(val, '\n\n' + tmpl + '\n\n');
    });

    // #parseModule()
    var childModules = ret.html.match(/\s#parseModule\(.*\)/g) || [];
    childModules.forEach(val => {
        var childName = /\(['"]([^'"]*)['"]\)/.exec(val)[1];
        var child = compileModule(childName, `${mpath.srcModule}${childName}`);
        ret.html = ret.html.replace(val, '\n\n' + child.html + '\n\n');

        ret.children = ret.children.concat(child.name, child.children);
        ret.cssList = ret.cssList.concat(child.cssList);
        ret.jsList = ret.jsList.concat(child.jsList);
    });

    return ret;
};

var layoutsObject = fs.readdirSync(mpath.srcLayout)
    .filter(v => fs.statSync(mpath.srcLayout + v).isDirectory())
    .reduce((prev, v) => {
        prev[v] = compileLayout(v, mpath.srcLayout + v);
        return prev;
    }, {});

var modulesObject = fs.readdirSync(mpath.srcModule)
    .filter(v => fs.statSync(mpath.srcModule + v).isDirectory())
    .reduce((prev, v) => {
        prev[v] = compileModule(v, mpath.srcModule + v);
        return prev;
    }, {});

var pagesObject = fs.readdirSync(mpath.srcPage)
    .filter(v => /\.conf$/.test(v))
    .reduce((prev, v) => {
        prev[v.substr(0, v.length - 5)] = vmHelper.getContext(vmHelper.getTemplate(mpath.srcPage + v));
        return prev;
    }, {});

var globalCxt = vmHelper.getContext(vmHelper.getTemplate(mpath.srcGlobalConf));

module.exports = { layoutsObject, modulesObject, pagesObject, globalCxt };
