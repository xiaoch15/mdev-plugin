const fs = require('fs');
// var render = require('./velocity.helper.js').render;

const replacePattern = (code, reg, resUrl, resVer, tail) => {
    /[\/\\]$/.test(resUrl) && (resUrl = resUrl.substring(0, resUrl.length - 1));
    return code.replace(reg,
        (match, url) => (/^[\/\\]/.test(url) ?
            (match.replace(url, resUrl + url + (resVer ? ('?' + resVer) : '')) + (tail ? tail(url) : '')) :
            match
        )
    );
};

// <img src=""
const proImageUrl = (html, resUrl, resVer) =>
    replacePattern(html, /<img.*?src=['"]([^'"]*)['"][^>]*>/gi, resUrl, resVer);

// @import ""
var proCssUrl = (css) => css.replace(/@import\s*['"]\//gi, '@import \"~');
    // replacePattern(css, /@import\s*['"]([^'"]*)['"];/gi, resUrl, null);

// url()
const proCssImgUrl = (css, resUrl, resVer) =>
    replacePattern(css, /url\(['"]?([^'"]*)['"]?\)/gi, resUrl, resVer);

const proCss = (fileFullName, resUrl, resVer) =>
    fs.writeFileSync(
        fileFullName,
        proCssImgUrl(proCssUrl(fs.readFileSync(fileFullName, 'utf8')), resUrl, resVer),
        'utf8'
    );

// // document.write('<script type="text/javascript" src=""></script>');
// const proJsUrl = (script, resUrl) =>
//     replacePattern(script, /document\.write\('<script.*?src=['"]([^'"]*)['"][^>]*><\/script>'\);/g, resUrl, null, function (url) {
//         return '/*[import]' + url + '[/import]*/';
//     });

// module.exports = { proImageUrl, proCssUrl, proJsUrl, proCssImgUrl, proCss, buildCssFile, buildJsFile, html2js };
module.exports = { proCss, proImageUrl };
